def fac(n):
    assert type(n) == int, '`n` should be an integer'
    assert n >=0, '`n` must by non-negative'
    if n==0:
        return 1
    prod = 1
    for i in range(2, n+1):
        prod = prod*i
    return prod


if __name__ == '__main__':
    for n in range(10):
        print('{}! = {}'.format(n, fac(n)))
